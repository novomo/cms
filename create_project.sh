create_project {
    package_name = $1
    package_lang = $2

    cms_folder = "$(realpath "$0" | sed 's|\(.*\)/.*|\1|')"
    if $package_name == "python"
    then
        (cp -r $(dirname "$currentFolder")/python_template $package_name)
    elif $package_name == "node"
        (cp -r $(dirname "$currentFolder")/node_template $package_name)
        (cd "$package_name" && yarn init)
    elif $package_name == "c"
    then
        (cp -r $(dirname "$currentFolder")/c_template $package_name)
    else
        echo "$package_lang is not yey supported" 
        return
    fi
    (cd "$package_name" && git init)
    
}