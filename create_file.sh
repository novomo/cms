create_file {
    file_name = $1
    package_lang = $2

    cms_folder = "$(realpath "$0" | sed 's|\(.*\)/.*|\1|')"
    if $package_name == "python"
    then
        (cp $(dirname "$currentFolder")/python_template_file.py $file_name.py)
    elif $package_name == "node"
        (cp $(dirname "$currentFolder")/node_template_file.js $file_name.js)
    elif $package_name == "c"
    then
        (cp $(dirname "$currentFolder")/c_template_file.cpp $file_name.cpp)
    else
        echo "$package_lang is not yey supported" 
        return
    fi
    (cd "$package_name" && git init)
    
}