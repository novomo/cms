#!/bin/bash
source "$(realpath "$0" | sed 's|\(.*\)/.*|\1|')/pull.sh"
source "$(realpath "$0" | sed 's|\(.*\)/.*|\1|')/create_project.sh"
source "$(realpath "$0" | sed 's|\(.*\)/.*|\1|')/create_file.sh"

if $1 == "-cp" || $1 == "--create-project"
then
  create_project $2 $3 #name and language
elif $1 == "-cf" || $1 == "--create-file"
then
  create_file $2 $3 #name and language
elif $1 == "-i" || $1 == "-install"
then
  install_package $2 $3 #package and type
elif $1 == "-s" || $1 == "-setup"
then
  setup_program
elif $1 == "-pu" || $1 == "--push"
then
  push
elif $1 == "-p" || $1 == "--pull"
then
  pull
elif $1 == "-m" || $1 == "--merge"
then
  merge_git $2 $3 #origin and branch to merge
elif $1 == "-h" || $1 == "--help"
then
  
  echo "-help\n"
  echo "-cp or --create-project <name of project> <language of project>\n"
  echo "-cf or --create-file <name of file> <language of file>\n"
  echo "-i or --install <name of package> <type of package>\n"
  echo "-s or --setup\n"
  echo "-pu or --push\n"
  echo "-p or --pull\n"
  echo "-m or --merge\n"

else
  echo "Your command was nnot recognised"
  echo ""
  echo "-help\n"
  echo "-cp or --create-project <name of project> <language of project>\n"
  echo "-cf or --create-file <name of file> <language of file>\n"
  echo "-i or --install <name of package> <type of package>\n"
  echo "-s or --setup\n"
  echo "-pu or --push\n"
  echo "-p or --pull\n"
  echo "-m or --merge\n"
fi











